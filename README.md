# node-express-rest-api-mysql8

A Basic Node.js/Express REST API implementation example.



# Prerequisites

For Windows

* Install mysql database (version 8 ) in your local pc 
* impor the provided mysql dump file 

![dump link](https://gitlab.com/jahurul-java/node-express-rest-api-mysql8/-/blob/main/doctorportalapi_user.sql)

# Usage

* Run `npm install` to installl dependencies
* Run `npm run start` to start the local server
* Load `http://localhost:8000` to test the endpoint. It will display a json result `{"message":"Ok"}`

# API Endpoints

## GET /api/users

Get a list of users

```json
{
    "message": "success",
    "data": [
        {
            "id": 1,
            "name": "jahurul islam",
            "email": "jahurul@yopmail.com",
            "password": "e10adc3949ba59abbe56e057f20f883e"
        },
        {
            "id": 2,
            "name": "shahidul islam",
            "email": "shaidul@yopmail.com",
            "password": "e10adc3949ba59abbe56e057f20f883e"
        },
        {
            "id": 3,
            "name": "manjurul islam",
            "email": "manjurul@yopmail.com",
            "password": "e10adc3949ba59abbe56e057f20f883e"
        }
    ]
}
```

## GET /api/user/{id}

Get user information by user id

```json
{
    "message": "success",
    "data": {
        "name": "amirul islam",
        "email": "amirul@yopmail.com",
        "password": "e10adc3949ba59abbe56e057f20f883e"
    }
}
```

## POST /api/user/

To create a new user based on POST data (x-www-form-url-encoded)

* name: User name
* email: User email
* password: User password

![Postman example](https://gitlab.com/jahurul-java/node-express-rest-api-mysql8/-/blob/main/PostMan-POST-request.png)


## PATCH /api/user/{id}

To update user data by id, based on POST data (x-www-form-url-encoded)

* name: User name
* email: User email
* password: User password

You can send only one attribute to update, the rest of the info remains the same. 

In this example, using CURL you can update the user email:

```bash
curl -X PATCH -d "email=user@example1.com" http://localhost:8000/api/user/2
```

## DELETE /api/user/{id}

To remove a user from the database by user id. 

This example is using the `curl` command line


```bash
curl -X "DELETE" http://localhost:8000/api/user/2
```

The result is:

`{"message":"deleted","rows":1}`











